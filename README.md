Unity.Extensions
================

This is the repository for the Hazware Unity Extension project.

- [Hazware Unity Extensions blog post](http://buksbaum.us/2009/02/08/type-tracking-extension-for-unity/)
- [Dave's blog](http://buksbaum.us/)

__Version 2.1 - 08/16/2011__
  * Cleaned up source tree
  * Added psake build script
  * Added Silverlight 4 support

__Version 2.0 - 06/16/2011__
  * Replaced static dependency to Unity with NuGet Package
  * Upgraded Unity 2.0.414 to 2.1.505
  * Created NuGet Package Hazware.Unity.TypeTracking
  * Renamed Directory, Namespace, Assembly name to Hazware.Unity.TypeTracking
  * Renamed Directory, Namespace, Assembly name to Hazware.Unity.TypeTracking.Tests
  * Added Hazware.Unity.TypeTracking-v40 to target .NET 4.0
  * Added Hazware.Unity.TypeTracking.Tests-v40 to target .NET 4.0
  * Changed unit testing from Gallio/MbUnit to NUnit

__Version 1.0 - 08/20/2009__
  * Initial Release
